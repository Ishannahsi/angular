import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tdf';
  topics = ['angular', 'react', 'vue'];
  topicHasError = true;

  userModel = new User('ishan', '', 456, 'default', 'Morning', true);

  validateTopic(value:any) {
    if (value === 'default') {
      this.topicHasError = true;
    } else {
      this.topicHasError = false;
    }
  }
}
